﻿using GeometryLibrary.Figure;

namespace FigureUnitTest
{
    [TestClass]
    public class UnitTestTriangle
    {
        [TestMethod]
        public void CalculateTriangleArea_ValidSides_ReturnsCorrectArea()
        {
            double sideA = 3;
            double sideB = 4;
            double sideC = 5;
            double expectedArea = 6;

            double actualArea = new Triangle(sideA, sideB, sideC).CalculateArea();

            Assert.AreEqual(expectedArea, actualArea, 0.001);
        }

        [TestMethod]
        public void TriangleIsRightTriangle_ValidSides_ReturnsTrue()
        {
            double sideA = 3;
            double sideB = 4;
            double sideC = 5;

            Triangle triangle = new Triangle(sideA, sideB, sideC);
            bool isRightTriangle = triangle.IsRightTriangle();

            Assert.IsTrue(isRightTriangle);
        }

        [TestMethod]
        public void TriangleIsRightTriangle_InvalidSides_ReturnsFalse()
        {
            double sideA = 3;
            double sideB = 4;
            double sideC = 6;

            Triangle triangle = new Triangle(sideA, sideB, sideC);
            bool isRightTriangle = triangle.IsRightTriangle();

            Assert.IsFalse(isRightTriangle);
        }
    }
}
