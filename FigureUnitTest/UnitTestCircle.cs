using GeometryLibrary.Figure;

namespace FigureUnitTest
{
    [TestClass]
    public class UnitTestCircle
    {
        [TestMethod]
        public void CalculateCircleArea_ValidRadius_ReturnsCorrectArea()
        {
            double radius = 5;
            double expectedArea = Math.PI * Math.Pow(radius, 2);

            double actualArea = new Circle(radius).CalculateArea();

            Assert.AreEqual(expectedArea, actualArea, 0.001);
        }
    }
}