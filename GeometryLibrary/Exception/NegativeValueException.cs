﻿namespace GeometryLibrary.Exception
{
    internal class NegativeValueException : ArgumentException
    {
        public NegativeValueException(string message, string parameter) 
            : base($"{message},{parameter}") {
        }

        public NegativeValueException(string message)
            : base($"{message}"){
        }
    }
}
