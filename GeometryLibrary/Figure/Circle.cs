﻿using GeometryLibrary.Exception;
using GeometryLibrary.Figure.Interface;

namespace GeometryLibrary.Figure
{
    public class Circle : IShape
    {
        private readonly double _radius;

        public Circle(double radius)
        {
            if (radius <= 0)
                throw new NegativeValueException("Радиус должен быть положительным числом.", nameof(radius));

            _radius = radius;
        }

        public double CalculateArea()
        {
            return Math.PI * Math.Pow(_radius, 2);
        }
    }
}
