﻿namespace GeometryLibrary.Figure.Interface
{
    public interface IShape
    {
        double CalculateArea();
    }
}
